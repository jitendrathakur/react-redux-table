
import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { DataGrid } from '@mui/x-data-grid';
import {selectedProduct, removeSelectedProduct} from "../redux/actions/productActions";

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'userId', headerName: 'User Id', width: 130 },
  { field: 'title', headerName: 'Title', width: 230 },
  {
    field: 'completed',
    headerName: 'Completed',
    type: 'boolean',
    width: 190,
  },
];

export default function ProductComponent() {
  const dispatch = useDispatch();
  let params = []
  const products = useSelector((state) => state.allProducts.products);
  const handleRowClick = (param) => {
    params = param;

  };

  useEffect(() => {
    if (params && params.length > 0)
          console.log(222222222222222)
          
        return () => (
          dispatch(removeSelectedProduct(removeSelectedProduct([params, products])))
        );
    }, [params]);

  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={products}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        checkboxSelection
        onRowClick={handleRowClick}
        onSelectionModelChange={handleRowClick}
      />
    </div>
  );
}
